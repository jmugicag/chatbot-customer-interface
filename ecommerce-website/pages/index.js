import Head from 'next/head'
import { Inter } from '@next/font/google'
import styles from '../styles/Home.module.css'
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import HeadBar from '../components/HeadBar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { ScopedCssBaseline } from '@mui/material';
import { useState } from 'react'
import Stack from '@mui/material/Stack';


const theme = createTheme();

export default function Home() {
  const[resultsFound,setResultsFound] = useState()
  console.log(["results found: ",resultsFound])

  return (
    <>
      <Head>
        <title>Chatbot Demo</title>
        <meta name="description" content="Chatbot explore" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ThemeProvider theme={theme}>
      <ScopedCssBaseline>
        <HeadBar />
        <main>
          {/* Hero unit */}
          <script src="https://www.gstatic.com/dialogflow-console/fast/df-messenger/prod/v1/df-messenger.js"></script>
          <df-messenger
            project-id="gab-ce-demos-2"
            agent-id="90ca972b-f914-4ea0-9741-a911e89a0a35"
            language-code="en">
            <df-messenger-chat-bubble
            chat-title="StellAIntis">
            </df-messenger-chat-bubble>
          </df-messenger>
        </main>
        </ScopedCssBaseline>
      </ThemeProvider>
    </>
  )
}
