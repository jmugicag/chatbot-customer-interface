export const getProductDetails = async (name) => {
    try {
        const res = await fetch('/api/product-details',{
            method: "POST",
            body: name
        });
	    const searchResult = JSON.parse(await res.json());
        //const element = fillInfo(searchResult.id,searchResult.images[0].uri,searchResult.description,searchResult.uri,searchResult.title)
        return searchResult;
    } catch (err) {
        console.log(err);
    }
  }