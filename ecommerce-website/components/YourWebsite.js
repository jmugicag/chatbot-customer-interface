import * as React from 'react';

function YourWebsite() {
  return (
      <div className="content" dangerouslySetInnerHTML={{__html:   
        'This is the test content'
      }}></div>
  );
}

export default YourWebsite;
